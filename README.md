cache simulator
===============

This project simulates a processor cache capable of simulating different configurations. It accepts programs made of several dozen different pseudo-assembly instructions, and outputs a count of numerous events such as cache misses.

## Build and Run

There are two make targets in the project:

* `make simulator` (default) - yields an executable named `simulator` that runs the programs as they're set in `main.c`.
* `make test` - yields an executable named `cache_test` that runs tests on accesses for caches of various sizes.

`simulator` takes four different command line options that offer control over the parameters of the simulated cache. All of the options look like unix "long" options and take an argument.

* `--associatiivity=x`: sets the set associativity to the integer `x`.
* `--blocksize=x`: sets the size of each block (or line) in the cache to `x` bytes. There's no input validation, so know that this expects something that's a power of 2. If you give it something else, you risk some undefined behavior.
* `--blockcount=x`: sets the nuber of blocks in the cache equal to the integer `x`.
* `--replacement=<opt>`: sets the replacement policy to use when a cache block has to be evicted. `<opt>` may be any one of the following:
 * `lru`: least-recently used replacement
 * `fifo`: use a "first-in first-out" queue to replace the oldest block fist
 * `rando`: replace blocks randomly

By default, the cache uses a write-through write policy and the parameters of the AMD Cortex A-8 processor. These are:

* 16kB size
* 64-bit blocks
* 256 blocks
* 4-way set associative
* random block replacement

## Analysis

Programs are written in a pseudo-assembly language. There are four different instructions:

* ld(address) - load double from address
* ad(value1, value2) - add two doubles
* mt(value1, value2) - multiply two doubles
* st(address, value) - store a double at an address

These instructions assume a 16-bit address space. The programs assume the values they need are already in memory. If they had to be stored first by the processor, the cache read statistics would get all messed up.

### dot product

Dot product works on two arrays of doubles. Doubles are 8 bytes, and the two arrays are stored sequentially in memory, for example:

* `A[0:1023]` - `0x00000000:0x00001ff8`
* `B[0:1023]` - `0x00002000:0x00003ff8`

The dot product tends to have a large number of compulsory misses. Because the data is all stored and acessed sequentially, we gain a lot from spatial locality. Once we're done with a location, it's gone forever. For caches with high enough associativity, we don't run into conflict misses until the cache is entirely full, at which point, they're capacity misses.

### matrix multiply

The matrix works on two matricies: one with dimensions 128 x 8 and the other with dimensions 8 x 128. These are stored in row-major order:

* `A[0][0]` - `0x00000000`
* `A[1][0]` - `0x00000100`
* `A[2][0]` - `0x00000200`
* `A[127][7]` - `0x00001ff8`
* `B[0][0]` - `0x00002000`
* `B[1][0]` - `0x00002400`
* `B[2][0]` - `0x00002800`
* `B[7][127]` - `0x00003ff8`

The matrix multiplication shows a more complicated relationship with the cache. The code is pretty naive, so we make good use of neither spatial nor temporal locality. As long as the matrix is large enough, there are typically a high number of both conflict and capacity misses in the run.

### Counting Misses

The definition of capacity vs conflict misses is a little shaky, and nothing seems to quite make sense for all levels of associativity. This project uses the following logic to determine the type of the miss:

```
if (cache is full) {
  return capacity_miss
} else if (this set is full) {
  return conflict_miss
} else {
  return compulsory_miss
}
```
