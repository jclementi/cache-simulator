.PHONY: clean test cache_tests

srcdir = cache_simulator/source
hdrdir = cache_simulator/headers
tstdir = spec
incdirs = $(hdrdir)

vpath %.c $(srcdir)
vpath %.h $(hdrdir)
vpath %_spec.% $(tstdir)

cc = gcc
cflags = -g -Wall -iquote $(incdirs)

default: simulator

test: cache_tests

cache_tests:
	$(cc) $(cflags) \
	  $(tstdir)/cache_spec.c \
		$(srcdir)/memory.c \
	  $(srcdir)/cache.c -o cache_tests \
	  -lcunit \
    -lm; \
	./cache_tests

simulator: io.c io.h cpu.c cpu.h cache.c cache.h main.c main.h memory.c memory.h address.h block.c block.h
	$(cc) $(cflags) -o simulator \
		$(srcdir)/cache.c \
		$(srcdir)/io.c \
		$(srcdir)/main.c \
		$(srcdir)/cpu.c \
		$(srcdir)/memory.c \
    -lm;

clean:
	-rm cache_tests simulator *.log; clear;
