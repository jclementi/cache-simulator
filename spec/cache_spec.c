#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <math.h>

#include "cache.h"
#include "memory.h"
#include "address.h"
#include "block.h"

// shared among the tests
memory_t *dram;

/* use a 16-bit address space */
const addr_t max_addr = 0xffff;

cache_t *cache_intel;
cache_t *cache_simple;
cache_t *cache_medium;
cache_t *cache_complex;
cache_t *cache_array[4];
int cache_count = 4;

// types of caches to run the tests with
// cache_params_t spec_intel is defined in `cache.c`
extern const cache_params_t spec_intel;

const cache_params_t spec_simple = (cache_params_t) {
    .associativity = 1, // direct-mapped
    .brstrat = rando,
    .wrstrat = through,
    .block_count = 64,
    .block_size = 32,
};

const cache_params_t spec_medium = (cache_params_t) {
    .associativity = 2,
    .brstrat = fifo,
    .wrstrat = through,
    .block_count = 128,
    .block_size = 32,
};

const cache_params_t spec_complex = (cache_params_t) {
    .associativity = 16,
    .brstrat = lru,
    .wrstrat = through,
    .block_count = 256,
    .block_size = 64,
};

cache_t *generate_empty_cache(cache_params_t params) {
    cache_t *cache = cache_create(params);
    block_t (*data)[] = cache->blocks;
    block_t empty_block = (block_t) {
        .head = 0x0000,
        .flags = empty_f,
        .tag = 0,
        .data = NULL,
    };

    int i;
    for (i = 0; i < cache->block_count; i++) {
        (*data)[i] = empty_block;
    }
    return cache;
}

cache_t *generate_full_cache(cache_params_t params, memory_t *memory) {
    cache_t *new_cache = cache_create(params);
    addr_t address_to_fetch;

    // generate addresses one cache line apart, then load them into the cache
    // until it's completely full
    int i;
    for (i = 0; i < new_cache->block_count; i++) {
        address_to_fetch = (addr_t) i * new_cache->block_size;
        cache_fetch_from_memory(new_cache, address_to_fetch, memory);
    }

    return new_cache;
}

/* start with a full cache and zero out half of it.
 * not efficient, but effective.
 */
cache_t *generate_med_full_cache(cache_params_t params, memory_t *memory) {
    cache_t *cache = generate_full_cache(params, memory);

    block_t (*data)[] = cache->blocks;
    block_t empty_block = (block_t) {
        .head = 0x0000,
        .flags = empty_f,
        .tag = 0,
        .data = NULL,
    };

    int i;
    for (i = cache->block_count/2; i < cache->block_count; i++) {
        (*data)[i] = empty_block;
    }

    return cache;
}

/* set up a few caches to use during the tests
 *
 * according to the cunit framework, setup tests should take no arguments and
 * return `0` on success
 */
int initialize_suite_empty(void) {
    dram = memory_init_and_fill(max_addr);

    cache_intel = generate_empty_cache(spec_intel);
    cache_simple = generate_empty_cache(spec_simple);
    cache_medium = generate_empty_cache(spec_medium);
    cache_complex = generate_empty_cache(spec_complex);

    return 0;
}

int initialize_suite_med_fill(void) {
    dram = memory_init_and_fill(max_addr);

    cache_intel = generate_med_full_cache(spec_intel, dram);
    cache_simple = generate_med_full_cache(spec_simple, dram);
    cache_medium = generate_med_full_cache(spec_medium, dram);
    cache_complex = generate_med_full_cache(spec_complex, dram);

    cache_array[0] = cache_intel;
    cache_array[1] = cache_simple;
    cache_array[2] = cache_medium;
    cache_array[3] = cache_complex;

    return 0;
}

int initialize_suite_full_fill(void) {
    dram = memory_init_and_fill(max_addr);

    cache_intel = generate_full_cache(spec_intel, dram);
    cache_simple = generate_full_cache(spec_simple, dram);
    cache_medium = generate_full_cache(spec_medium, dram);
    cache_complex = generate_full_cache(spec_complex, dram);

    cache_array[0] = cache_intel;
    cache_array[1] = cache_simple;
    cache_array[2] = cache_medium;
    cache_array[3] = cache_complex;

    return 0;
}

int clean_suite(void) {
    memory_free(dram);

    cache_free(cache_intel);
    cache_free(cache_simple);
    cache_free(cache_medium);
    cache_free(cache_complex);

    return 0;
}

/* test functions */

void load_existing(void) {
    cache_t *cache = NULL;
    addr_t address = 0x0050;
    addr_t expected_block_head;
    cache_result_t result;

    for (int i = 0; i < cache_count; i++) {
        cache = cache_array[i];
        result = cache_load_addr(cache, address, dram);

        expected_block_head = address - (address % cache->block_size);

        CU_ASSERT_DOUBLE_EQUAL(result.requested_double, (double) 0xa, .01);
        CU_ASSERT_EQUAL(result.block.head, expected_block_head);
    }

    return;
}

void load_missing(void) {
    // set up empty cache
    cache_t *cache = NULL;
    addr_t address = 0xfff8;
    addr_t expected_block_head;
    cache_result_t result;

    for (int i = 0; i < cache_count; i++) {
        cache = cache_array[i];

        expected_block_head = address - (address % cache->block_size);
        result = cache_load_addr(cache, address, dram);

        CU_ASSERT_EQUAL(result.hit, 0);
        CU_ASSERT_EQUAL(result.miss_type, compulsory);
        CU_ASSERT_EQUAL(result.block.head, expected_block_head);
        CU_ASSERT_DOUBLE_EQUAL(result.requested_double, (double) (0x1fff), .01);
    }

    return;
}

void ref_hit(void) {
    // set up full cache
    cache_t *cache = NULL;
    addr_t address = 0x0080;
    addr_t expected_block_head;
    cache_result_t result;

    for (int i = 0; i < cache_count; i++) {
        cache = cache_array[i];

        expected_block_head = address - (address % cache->block_size);
        result = cache_load_addr(cache, address, dram);

        CU_ASSERT_EQUAL(result.block.head, expected_block_head);
        CU_ASSERT_DOUBLE_EQUAL(result.requested_double, (double) 0x0010, .01);
        CU_ASSERT_EQUAL(result.hit, 1);
    }

    return;
}

void compulsory_miss(void) {
    // set up empty cache
    // make unsuccessful reference
    // log the miss
    cache_t *cache = NULL;
    addr_t address = 0xfff8;
    addr_t expected_block_head;
    cache_result_t result;

    for (int i = 0; i < cache_count; i++) {
        cache = cache_array[i];

        expected_block_head = address - (address % cache->block_size);
        result = cache_load_addr(cache, address, dram);

        CU_ASSERT_EQUAL(result.hit, 0);
        CU_ASSERT_EQUAL(result.block.head, expected_block_head);
        CU_ASSERT_DOUBLE_EQUAL(result.requested_double, (double)0x1fff, .01);
        CU_ASSERT_EQUAL(result.miss_type, compulsory);
    }
    return;
}

void conflict_miss(void) {
    // set up half-full cache
    // reference block for occupied space
    // loc conflict miss
    cache_t *cache = NULL;
    addr_t address = 0x8100;
    addr_t expected_block_head;
    cache_result_t result;

    for (int i = 0; i < cache_count; i++) {
        cache = cache_array[i];

        expected_block_head = address - (address % cache->block_size);
        result = cache_load_addr(cache, address, dram);

        CU_ASSERT_EQUAL(result.hit, 0);
        CU_ASSERT_EQUAL(result.block.head, expected_block_head);
        CU_ASSERT_DOUBLE_EQUAL(result.requested_double, (double) 0x1020, .01);
        CU_ASSERT_EQUAL(result.miss_type, conflict);
    }
    return;
}

void capacity_miss(void) {
    // set up full cache
    // make miss reference
    // log capacity miss
    cache_t *cache = NULL;
    addr_t address = 0xff00;
    addr_t expected_block_head;
    cache_result_t result;

    for (int i = 0; i < cache_count; i++) {
        cache = cache_array[i];

        result = cache_load_addr(cache, address, dram);
        expected_block_head = address - (address % cache->block_size);

        CU_ASSERT_EQUAL(result.hit, 0);
        CU_ASSERT_EQUAL(result.miss_type, capacity);
        CU_ASSERT_DOUBLE_EQUAL(result.requested_double, (double) 0x1fe0, .01);
        CU_ASSERT_EQUAL(result.block.head, expected_block_head);
    }
    return;
}

void replacement_lru(void) {
    // use the intel cache
    // fill one set
    // access each element twice
    // read a new element that should hit the set
    // make sure lru block gets replaced
    cache_result_t result;
    // 6 offset bits, 32 sets -> 5 set index bits, top 5 bits all map to same set
    result = cache_load_addr(cache_intel, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0800, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x2000, dram);

    // accessing
    result = cache_load_addr(cache_intel, (addr_t) 0x0800, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x2000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x2000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0800, dram);

    // load new one into the set
    result = cache_load_addr(cache_intel, (addr_t) 0x4000, dram);

    // make sure `0x1000` was replaced
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    CU_ASSERT_EQUAL(result.hit, 0);
    CU_ASSERT_EQUAL(result.miss_type, conflict);

    // do it again
    // accessing
    result = cache_load_addr(cache_intel, (addr_t) 0x0800, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x2000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x2000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_intel, (addr_t) 0x0800, dram);

    // load new one into the set
    result = cache_load_addr(cache_intel, (addr_t) 0x4000, dram);

    // make sure `0x1000` was replaced
    result = cache_load_addr(cache_intel, (addr_t) 0x1000, dram);
    CU_ASSERT_EQUAL(result.hit, 0);
    CU_ASSERT_EQUAL(result.miss_type, conflict);

    return;
}

void replacement_fifo(void) {
    // use medium spec
    // 5 offset bits, 64 sets->6 index bits, top 5 bits map to same set
    // make sure lru block gets replaced
    cache_result_t result;
    // setting up
    result = cache_load_addr(cache_medium, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x0800, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x2000, dram);

    // accessing (shouldn't affect which block is replaced
    result = cache_load_addr(cache_medium, (addr_t) 0x0800, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x2000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x1000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x2000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x0000, dram);
    result = cache_load_addr(cache_medium, (addr_t) 0x0800, dram);

    // access a new block
    result = cache_load_addr(cache_medium, (addr_t) 0x4000, dram);

    // make sure `0x0000` was replaced
    result = cache_load_addr(cache_medium, (addr_t) 0x0000, dram);
    CU_ASSERT_EQUAL(result.hit, 0);
    CU_ASSERT_EQUAL(result.miss_type, conflict);

    return;
}

void replacement_random(void) {
    return;
}



int main(void)
{
    CU_initialize_registry();
    CU_pSuite reference = CU_add_suite("cache retrieves correct data", initialize_suite_med_fill, clean_suite);
    CU_add_test(reference, "identifies a block loaded into the cache", load_existing);
    CU_add_test(reference, "misses a block not loaded into the cache", load_missing);
    CU_pSuite med_full_result = CU_add_suite("med-fill cache counts correct event", initialize_suite_med_fill, clean_suite);
    CU_add_test(med_full_result, "counts hit when a reference is in the cache", ref_hit);
    CU_add_test(med_full_result, "counts compulsory miss when filling an empty block", compulsory_miss);
    CU_add_test(med_full_result, "counts conflict miss when replacing a block and the cache is not full", conflict_miss);
    CU_pSuite full_fill_result = CU_add_suite("full-fill cache counts correct event", initialize_suite_full_fill, clean_suite);
    CU_add_test(full_fill_result, "counts capacity miss when replacing a block and the cache is full", capacity_miss);

    CU_pSuite replacement = CU_add_suite("cache replaces the correct block", initialize_suite_empty, clean_suite);
    CU_add_test(replacement, "replaces least-recently-used block with LRU strategy on capacity miss", replacement_lru);
    CU_add_test(replacement, "replaces oldest block with fifo strategy on capacity miss", replacement_fifo);
    CU_add_test(replacement, "replaces any block with random strategy on capaticy miss", replacement_random);
    CU_basic_run_tests();
    //CU_console_run_tests();
    CU_cleanup_registry();
    return 0;
}
