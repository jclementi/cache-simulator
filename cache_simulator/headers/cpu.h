#ifndef CPU_H
#define CPU_H

#include <stdio.h>

#include "address.h"
#include "cache.h"

/* holds the data that we report on at the end of the program
*/
typedef struct program_statistics {
    unsigned int total_hit_count;
    unsigned int total_miss_count;
    unsigned int comp_miss_count;
    unsigned int conf_miss_count;
    unsigned int capy_miss_count;
    unsigned int write_hit_count;
    unsigned int write_miss_count;
    unsigned int read_hit_count;
    unsigned int read_miss_count;
    unsigned int instruction_count;
} prog_stats_t;

/* these are the pseudo instructions that are used to code the dot product
 * and matrix multiply algorithms
 */
double ld(memory_t *, cache_t *, addr_t, FILE *, FILE *, prog_stats_t *);
double ad(double, double, FILE *, prog_stats_t *);
double mt(double, double, FILE *, prog_stats_t *);
int st(memory_t *, cache_t *, addr_t, double, FILE *, FILE *, prog_stats_t *);

void update_stats(cache_result_t *, prog_stats_t *);

typedef struct memory_matrix {
    addr_t head;
    int row_count;
    int col_count;
} mem_matrix_t;

double dot_product(memory_t *, cache_t *, addr_t, addr_t, int, FILE *, FILE *, prog_stats_t *);
mem_matrix_t matrix_multiply(memory_t *, cache_t *, mem_matrix_t, mem_matrix_t, FILE *, FILE *, prog_stats_t *);

#endif // CPU_H
