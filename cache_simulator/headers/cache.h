#ifndef CACHE_H
#define CACHE_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "block.h"
#include "address.h"
#include "memory.h"

/* associativity typedef
 * 0 - fully associative
 * 1 - direct mapped
 * n - n-way associative
 *
 * In a cache with n blocks, n and 0 will both yield fulle associative caches
 */
typedef int assoc_t;

/* block replacement strategy
 * configurable at runtime through command-line options
 */
typedef enum block_replacement {
    lru = 0,
    fifo = 1,
    rando = 2
} brstrat_e;

/* write strategy selection
 * project only requires write-through
 * including for completeness
 */
typedef enum write_strategy {
    through,
    back
} wrstrat_e;


/* the cache contains all the information any function should need to know
 * in order to deal with it properly. this includes the associativity, the
 * block replacement strategy, the write strategy, the number of blocks,
 * the size of each block, a pointer to the array of all blocks, and a
 * pointer to the data structure needed for the block replacement strategy
 */
typedef struct cache {
    assoc_t associativity;
    wrstrat_e wrstrat;
    brstrat_e brstrat;
    unsigned int block_count;
    unsigned int block_size;
    block_t (*blocks)[];
} cache_t;

/* the cache params struct holds parameters needed to specify a new cache.
 * it's accepted by the cache_create function, which returns a pointer to a
 * cache that matches the specification in the params struct
 */
typedef struct cache_params {
    assoc_t associativity;
    brstrat_e brstrat;
    wrstrat_e wrstrat;
    int block_count;
    int block_size;
} cache_params_t;

/* default cache setting for the intel processor
 *
 * this is used by the spec files and `main.c` file to get sensible defaults
 */
extern const cache_params_t spec_intel;

typedef enum miss_category {
    compulsory
        ,	conflict
        ,	capacity
        ,	mystery // used to stub out tests
} miss_e;

/* cache_result_t 
 *
 * holds the result of a lookup operation. it contains the requested memory
 * block, whether the lookup hit or missed, and if it missed, it holds the
 * type of miss.
 */
typedef struct cache_result {
    int hit; // boolean
    miss_e miss_type;
    block_t block;
    double requested_double;
    addr_t requested_address;
} cache_result_t;

cache_t *cache_create(cache_params_t);
void cache_free(cache_t *);
cache_result_t cache_load_addr(cache_t *, addr_t, memory_t *);
cache_result_t cache_store_addr(cache_t *, addr_t, double, memory_t *);
block_t cache_fetch_from_memory(cache_t *, addr_t, memory_t *);

/* functions to find a block to evict
*/
block_t *find_lru(cache_t *, int);
block_t *find_oldest(cache_t *, int);
block_t *find_random(cache_t *, int);

#endif // CACHE_H
