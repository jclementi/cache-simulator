#ifndef ADDRESS_H
#define ADDRESS_H

/* addresses are 64-bit integers (long long ints). the thing consuming the
 * address is responsible for knowing whether the address is valid and
 * consuming only valid address bits.
 */

typedef unsigned long long int addr_t;

#endif // ADDRESS_H
