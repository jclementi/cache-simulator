#ifndef IO_H
#define IO_H

#include "cache.h"

void set_associativity(cache_params_t *, char *);
void set_blocksize(cache_params_t *, char *);
void set_blockcount(cache_params_t *, char *);
void set_replacement(cache_params_t *, char *);

#endif //IO_H
