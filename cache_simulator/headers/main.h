#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "cache.h"
#include "memory.h"
#include "io.h"
#include "cpu.h"

extern const cache_params_t spec_intel;

#endif // MAIN_H
