#ifndef MEMORY_H
#define MEMORY_H

/* virtual memory implementation. includes no page tables, address translation
 * or translation lookaside buffers. this is just a place for the cache to go
 * to fetch memory it doesn't already have. 
 *
 * memory will be a wide-open pointer space, so calling it an array of
 * characters to make sure each byte can be indexed.
 */

typedef struct virtual_memory {
    double (*data)[];
    addr_t max_addr;
} memory_t; 


memory_t *memory_init(addr_t);
void memory_free(memory_t *);
memory_t *memory_init_and_fill(addr_t);
block_t memory_fetch(addr_t, memory_t *);

#endif // MEMORY_H
