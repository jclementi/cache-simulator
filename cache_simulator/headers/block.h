#ifndef BLOCK_H
#define BLOCK_H

#include "address.h"

/* describes the block of memory returned by the cache to the processor. the
 * cache itself is an array of blocks, all of which start out empty.
 *
 * the size of the block is determined by command-line options, so the space
 * for the data must be dynamically allocated.
 *
 * `head` is the first address represented in the block
 *
 * the `flags` member contains a bitfield with flags describing the data in
 * the block. for example, the 'dirty' bit for writes and 'empty' bit to
 * represent empty spaces in the cache.
 *
 * the cache is responsible for knowing the size of the data lines in each 
 * block.
 */

/* values for the flags
 *
 * empty - hasn't been written to or read from. replacing and empty block
 *         will be a compulsory miss
 * valid - is safe to read from
 * dirty - has been written to. need to write back before evicting
 */
typedef enum block_flags {
    empty_f,
    valid_f,
    dirty_f
} flags_e;

typedef struct block {
    addr_t head;
    flags_e flags;
    unsigned int replacement_rank;
    unsigned long long int tag;
    double (*data)[];
} block_t;

#endif // BLOCK_H
