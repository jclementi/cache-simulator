#include "cpu.h"

/* cpu knows the algorithms that it needs to run already. `main` will call
 * the cpu and ask it to run a program on memory. `cpu` will initialize the
 * dram with the values needed, then run the pseudo-instructions that
 * perform the algorithm.
 *
 * the cpu also knows about the open logfiles that `main` created and the
 * program stats counter structure, this is questionable practice, but
 * effective for a school project
 */

double ld(memory_t *memory, cache_t *cache, addr_t address, FILE *cache_log, FILE *exec_log, prog_stats_t *stats) {
    cache_result_t result;
    result = cache_load_addr(cache, address, memory);

    if (result.hit) {
        stats->total_hit_count += 1;
        stats->read_hit_count += 1;
        fprintf(cache_log, "load,hit,%x,%.f\n", (int) address, result.requested_double);
    } else {
        char miss_type[11];
        stats->total_miss_count += 1;
        stats->read_miss_count += 1;
        switch (result.miss_type) {
            case compulsory: {
                stats->comp_miss_count += 1;
                sprintf(miss_type, "compulsory");
                break; }
            case conflict: {
                stats->conf_miss_count += 1;
                sprintf(miss_type, "conflict");
                break; }
            case capacity: {
                stats->capy_miss_count += 1;
                sprintf(miss_type, "capacity");
                break; }
            default: { printf("bad miss type: %d", result.miss_type); exit(1); }
        }

        fprintf(cache_log, "load,%s miss,%x,%.f\n", miss_type, (int) address, result.requested_double);
    }

    fprintf(exec_log, "load,%x,%.f\n", (int) address, result.requested_double);

    return result.requested_double;
}

int st(memory_t *memory, cache_t *cache, addr_t address, double a, FILE *cache_log, FILE *exec_log, prog_stats_t *stats) {
    cache_result_t result;
    result = cache_store_addr(cache, address, a, memory);

    if (result.hit) {
        stats->total_hit_count += 1;
        stats->write_hit_count += 1;
        fprintf(cache_log, "store,hit,%x,%.f\n", (int) address, a);
        fprintf(exec_log, "store,%x %.f,hit\n", (int) address, a);
    } else {
        stats->total_miss_count += 1;
        stats->write_miss_count += 1;
        char miss_type[11];
        switch (result.miss_type) {
            case compulsory: {
                stats->comp_miss_count += 1;
                sprintf(miss_type, "compulsory");
                break; }
            case conflict: {
                stats->conf_miss_count += 1;
                sprintf(miss_type, "conflict");
                break; }
            case capacity: {
                stats->capy_miss_count += 1;
                sprintf(miss_type, "capacity");
                break; }
            default: { printf("bad miss type: %d", result.miss_type); exit(1); }
        }

        fprintf(cache_log, "load,%s miss,%x,%.f\n", miss_type, (int) address, result.requested_double);
        fprintf(exec_log, "store,%x %.f,%s miss\n", (int) address, a, miss_type);
    }

    return 0;
}

double ad(double a, double b, FILE *exec_log, prog_stats_t *stats) {
    stats->instruction_count += 1;

    fprintf(exec_log, "add,%.f %.f,%.f\n", a, b, a+b);
    return a+b;
}

double mt(double a, double b, FILE *exec_log, prog_stats_t *stats) {
    stats->instruction_count += 1;
    fprintf(exec_log, "mult,%.f %.f,%.f\n", a, b, a*b);
    return a*b;
}


/* dot-product algorithm
 *
 * takes two arrays of the same size. at each step through the arrays,
 * one element from each array is loaded. the two loaded elements are
 * multiplied with one another, and the result is added to the current
 * accumulating value, and the accumulating value is stored.
 *
 * Note we could proceed by keeping the accumulating value in a 'register',
 * dramatically reducing the number of writes. I did it this way to better
 * simulate the common method of coding the dot product where each product
 * is added to a variable, which is written to memory at every iteration.
 *
 * e.g. - `acc` is stored to memory in every iteration here
 * for (int i = 0; i < size; i++) {
 *   acc += a[i] * b[i]
 * }
 *
 * */
double dot_product(memory_t *memory
        , cache_t *cache
        , addr_t head_a
        , addr_t head_b
        , int size
        , FILE *cache_log
        , FILE *exec_log
        , prog_stats_t *stats) {
    double result = 0;

    double reg_acc = 0
        , reg_op1 = 0
        , reg_op2 = 0;

    addr_t addr_acc, addr_op1, addr_op2;

    // want to store the accumulator immediately after the second array. to do
    // this 'right' we'd have to implement malloc with a simulated virtual
    // memory system. not doing that today
    addr_acc = head_b + (size * 8) + 8;

    for (int i = 0; i < size; i++) {
        // find the address of the operands
        addr_op1 = head_a + (i * 8);
        addr_op2 = head_b + (i * 8);

        // load the operands
        reg_op1 = ld(memory, cache, addr_op1, cache_log, exec_log, stats);
        reg_op2 = ld(memory, cache, addr_op2, cache_log, exec_log, stats);

        // multiply them
        // add them to the accumulator
        reg_acc += mt(reg_op1, reg_op2, exec_log, stats);
        st(memory, cache, addr_acc, reg_acc, cache_log, exec_log, stats);
    }
    result = ld(memory, cache, addr_acc, cache_log, exec_log, stats);

    return result;
}

/* matrix-multiply algorithm */
mem_matrix_t matrix_multiply(memory_t *memory
        , cache_t *cache
        , mem_matrix_t a
        , mem_matrix_t b
        , FILE *cache_log
        , FILE *exec_log
        , prog_stats_t *stats) {

    // the result matrix will immediately follow the second matrix, and the
    // 'accumulator' variable will follow the result matrix.
    double reg_acc = 0
        , reg_op1 = 0
        , reg_op2 = 0;

    addr_t addr_acc
        , addr_result_matrix
        , addr_matrix_ptr
        , addr_op1
        , addr_op2;

    addr_result_matrix = b.head + (b.row_count * b.col_count * 8) + 8;

    mem_matrix_t result = {
        .head = addr_result_matrix,
        .row_count = a.row_count,
        .col_count = b.col_count
    };

    addr_acc = result.head + (result.row_count * result.col_count * 8) + 8;

    // naive matrix multiplication method
    // for every row in matrix a
    for (int i = 0; i < a.row_count; i++) {
        // for every column in matrix b
        for (int j = 0; j < b.col_count; j++) {
            // perform the dot product and store the result
            for (int k = 0; k < b.row_count; k++) {
                addr_op1 = a.head + (a.col_count * i * 8) + (k * 8);
                addr_op2 = b.head + (b.col_count * k * 8) + (j * 8);

                reg_op1 = ld(memory, cache, addr_op1, cache_log, exec_log, stats);
                reg_op2 = ld(memory, cache, addr_op2, cache_log, exec_log, stats);

                reg_acc += mt(reg_op1, reg_op2, exec_log, stats);
                st(memory, cache, addr_acc, reg_acc, cache_log, exec_log, stats);
            }

            // put result into the result matrix
            addr_matrix_ptr = result.head + (i * result.col_count * 8) + (j * 8);
            reg_acc = ld(memory, cache, addr_acc, cache_log, exec_log, stats);
            st(memory, cache, addr_matrix_ptr, reg_acc, cache_log, exec_log, stats);
            reg_acc = 0;
        }
    }

    return result;
}
