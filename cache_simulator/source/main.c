#include "main.h"

/* controls the program and drives the io. plays the role of the 'processor'
 * in the simulation.
 */

int main(int argc, char *argv[]) {

    cache_params_t cache_params = spec_intel;
    addr_t max_addr = 0xffff;

    // accept command-line options to set the parameters for the cache. note
    // only long-style options are accepted
    //
    // --associativity; accepts an integer
    // --blocksize; accepts an integer
    // --blockcount; accepts an integer
    // --replacement; accepts "lru", "random", or "fifo"

    static struct option long_options[] = {
        {"associativity", optional_argument, NULL, 'a'},
        {"blocksize", optional_argument, NULL, 'b'},
        {"blockcount", optional_argument, NULL, 'c'},
        {"replacement", optional_argument, NULL, 'r'},
        {0, 0, 0, 0}
    };

    int c;
    int option_index = 0;

    while(1) {
        c = getopt_long(argc, argv, "", long_options, &option_index);

        if (c == -1) { break; }

        switch (c) {
            case 'a': set_associativity(&cache_params, optarg); break;
            case 'b': set_blocksize(&cache_params, optarg); break;
            case 'c': set_blockcount(&cache_params, optarg); break;
            case 'r': set_replacement(&cache_params, optarg); break;
            default: {
                printf("unrecognized option: %s\n", optarg);
                exit(1);
            }
        }
    }

    // print out generated cache params struct to verify
    char rep[6];
    switch (cache_params.brstrat) {
        case lru: { sprintf(rep, "lru"); break; }
        case fifo: { sprintf(rep, "fifo"); break; }
        case rando: { sprintf(rep, "rand"); break; }
    }

    printf("cache parameters:\n"
            "\tassociativity: %d\n"
            "\tblock size: %d\n"
            "\tblock count: %d\n"
            "\treplacement strat: %s\n"
            , cache_params.associativity
            , cache_params.block_size
            , cache_params.block_count
            , rep);

    // create cache, memory
    cache_t *g_cache = cache_create(cache_params);
    memory_t *g_dram = memory_init_and_fill(max_addr);
    prog_stats_t *g_stats = calloc(1, sizeof(prog_stats_t));

    // open logfile, init counting structs
    FILE *cache_log = fopen("cache_operations.log", "w+");
    FILE *exec_log = fopen("program_execution.log", "w+");
    FILE *stats_dot_log = fopen("graph_stats_dot.log", "a");
    FILE *stats_mxm_log = fopen("graph_stats_mxm.log", "a");

    fprintf(cache_log, "op,success,address,double");
    fprintf(exec_log, "op,operands,result");
    //fprintf(stats_dot_log, "block_count,block_size,cache_size,replacement_policy,associativity,total_hits,total_misses,compulsory,conflict,capacity,write_hits,write_misses\n");
    //fprintf(stats_mxm_log, "block_count,block_size,cache_size,replacement_policy,associativity,total_hits,total_misses,compulsory,conflict,capacity,write_hits,write_misses\n");

    // execute program
    double dp_result = dot_product(g_dram, g_cache, 0x0010, 0x1010, 10, cache_log, exec_log, g_stats);
    fprintf(cache_log, "// end of dot product\n\n");
    fprintf(exec_log, "// end of dot product\n\n");
    fprintf(stats_dot_log, "%d,%d,%d,%s,%d,%d,%d,%d,%d,%d,%d,%d\n"
            , cache_params.block_count
            , cache_params.block_size
            , cache_params.block_size * cache_params.block_count
            , rep
            , cache_params.associativity
            , g_stats->read_hit_count
            , g_stats->read_miss_count
            , g_stats->comp_miss_count
            , g_stats->conf_miss_count
            , g_stats->capy_miss_count
            , g_stats->write_hit_count
            , g_stats->write_miss_count);


    printf("result of dot product: %f\n", dp_result);

    // print resulting counts
    printf("execution stats:\n"
            "\ttotal hits: %d\n"
            "\ttotal misses: %d\n"
            "\t\tcompulsory misses: %d\n"
            "\t\tconflict misses  : %d\n"
            "\t\tcapacity misses  : %d\n"
            "\tread hits: %d\n"
            "\tread misses: %d\n"
            "\twrite hits: %d\n"
            "\twrite misses: %d\n"
            "\tinstruction count: %d\n"
            , g_stats->total_hit_count
            , g_stats->total_miss_count
            , g_stats->comp_miss_count
            , g_stats->conf_miss_count
            , g_stats->capy_miss_count
            , g_stats->read_hit_count
            , g_stats->read_miss_count
            , g_stats->write_hit_count
            , g_stats->write_miss_count
            , g_stats->instruction_count);

    // build memory matricies, then run
    // simple matricies
    /*
       mem_matrix_t mat_a = {
       .head = 0x0008,
       .row_count = 2,
       .col_count = 4
       };

       mem_matrix_t mat_b = {
       .head = 0x0048,
       .row_count = 4,
       .col_count = 2,
       };
     */
    /* [ 1  2  3  4 ]     [ 9  10 ]     [ 130  140 ]
     * [ 5  6  7  8 ]  x  [ 11 12 ]  =  [ 322  348 ]
     *                    [ 13 14 ]
     *                    [ 15 16 ]
     */
    // large matricies
    mem_matrix_t mat_c = {
        .head = 0x0400,
        .row_count = 8,
        .col_count = 64
    };

    mem_matrix_t mat_d = {
        .head = 0x4400,
        .row_count = 64,
        .col_count = 10
    };

    mem_matrix_t mxm_result = matrix_multiply(g_dram, g_cache, mat_c, mat_d, cache_log, exec_log, g_stats);

    printf("result of matrix multiply:\n");
    for (int i = 0; i < mxm_result.row_count; i++) {
        printf("[ ");
        for (int j = 0; j < mxm_result.col_count; j++) {
            printf(" %-8.0f ", *((double *)g_dram->data + (mxm_result.head/8) + (mxm_result.col_count * i) + j));
        }
        printf(" ]\n");
    }

    fprintf(cache_log, "// end of matrix multiply\n\n");
    fprintf(exec_log, "// end of matrix multiply\n\n");
    fprintf(stats_mxm_log, "%d,%d,%d,%s,%d,%d,%d,%d,%d,%d,%d,%d\n"
            , cache_params.block_count
            , cache_params.block_size
            , cache_params.block_size * cache_params.block_count
            , rep
            , cache_params.associativity
            , g_stats->read_hit_count
            , g_stats->read_miss_count
            , g_stats->comp_miss_count
            , g_stats->conf_miss_count
            , g_stats->capy_miss_count
            , g_stats->write_hit_count
            , g_stats->write_miss_count);

    // print resulting counts
    printf("execution stats:\n"
            "\ttotal hits: %d\n"
            "\ttotal misses: %d\n"
            "\t\tcompulsory misses: %d\n"
            "\t\tconflict misses  : %d\n"
            "\t\tcapacity misses  : %d\n"
            "\tread hits: %d\n"
            "\tread misses: %d\n"
            "\twrite hits: %d\n"
            "\twrite misses: %d\n"
            "\tinstruction count: %d\n"
            , g_stats->total_hit_count
            , g_stats->total_miss_count
            , g_stats->comp_miss_count
            , g_stats->conf_miss_count
            , g_stats->capy_miss_count
            , g_stats->read_hit_count
            , g_stats->read_miss_count
            , g_stats->write_hit_count
            , g_stats->write_miss_count
            , g_stats->instruction_count);
    // free cache, memory, close files
    fclose(cache_log);
    fclose(exec_log);
    cache_free(g_cache);
    memory_free(g_dram);

    return 0;
}
