#include <stdlib.h>
#include <string.h>

#include "io.h"

void set_associativity(cache_params_t *c_params, char *associativity) {
    assoc_t parsed_associativity = (assoc_t) strtol(associativity, NULL, 10);
    c_params->associativity = parsed_associativity;
    return;
}

void set_blocksize(cache_params_t *c_params, char *blocksize) {
    int parsed_blocksize = (int) strtol(blocksize, NULL, 10);
    c_params->block_size = parsed_blocksize;
    return;
}

void set_blockcount(cache_params_t *c_params, char *blockcount) {
    int parsed_blockcount = (int) strtol(blockcount, NULL, 10);
    c_params->block_count = parsed_blockcount;
    return;
}

void set_replacement(cache_params_t *c_params, char *strat) {
    int cmp_result;
    char *strats[3];

    // the values here are in sync with the values of these options
    // in the `brstrat_e` enum
    strats[0] = "lru";
    strats[1] = "fifo";
    strats[2] = "random";

    for (int i = 0; i < 3; i++) {
        cmp_result = strcmp(strat, strats[i]);
        if (cmp_result == 0) {
            c_params->brstrat = i;
            break;
        }
    }

    return;
}
