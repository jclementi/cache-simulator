#include "cache.h"

/* default settings references elsewhere
 */
const cache_params_t spec_intel = (cache_params_t) {
    .associativity = 4,
    .brstrat = lru,
    .wrstrat = through,
    .block_count = 128,
    .block_size = 64
};

cache_t *cache_create(cache_params_t cp) {
    cache_t *cache = malloc(sizeof(cache_t));

    *cache = (cache_t) {
        .associativity = cp.associativity,
        .brstrat = cp.brstrat,
        .wrstrat = cp.wrstrat,
        .block_count = cp.block_count,
        .block_size = cp.block_size
    };

    cache->blocks = (block_t (*) []) malloc(sizeof(block_t) * cp.block_count);

    return cache;
}

void cache_free(cache_t *cache) {
    free(cache->blocks);
    free(cache);
    return;
}

// assumes configurations don't lead to aliasing
cache_result_t cache_load_addr(cache_t *cache, addr_t address, memory_t *memory) {
    cache_result_t result;
    block_t block;

    block.head = 0x0000;
    block.flags = empty_f;
    block.tag = 0;
    block.data = (double (*) []) &block;

    // offset determined by the block size
    // 64 (0x01000000), 63 (0x00111111) <- offset mask
    int offset_mask = cache->block_size - 1;
    addr_t offset = offset_mask & address;
    double offset_bits_d = log2((double) cache->block_size);
    int offset_bits = round(offset_bits_d);

    // number of sets
    int set_count = cache->block_count / cache->associativity;
    int index_mask = set_count - 1;

    addr_t index = address >> offset_bits;
    index &= index_mask;

    // tag
    int tag_mask = ~offset_mask;
    int tag = address & tag_mask;

    // have the index and offset, check the cache for a matching tag
    // index to the appropriate set, search the blocks
    int block_index = cache->associativity * index;
    block_t *block_cur;

    for (int i = 0; i < cache->associativity; i++) {
        block_cur = (block_t *)cache->blocks + (block_index + i);
        if (tag == block_cur->tag
                && (block_cur->flags == valid_f)) {
            // on hit, update replacement rank if it's an lru cache
            // bring current block to the head of the list
            // push all blocks with higher replacement rank down
            if (cache->brstrat == lru) {
                block_t *b;
                for (int j = 0; j < cache->associativity; j++) {
                    b = (block_t *)cache->blocks + block_index + j;
                    if (b->replacement_rank > block_cur->replacement_rank) {
                        b->replacement_rank -= 1;
                    }
                }
                block_cur->replacement_rank = cache->associativity;
            }
            return (cache_result_t) {
                .hit = 1,
                    .block = *block_cur,
                    .requested_double = *(double *)((double *)block_cur->data + (offset/8)),
                    .requested_address = address
            };
        }
    }

    // no match found
    // determine miss type
    // if set is full, it's a conflict miss
    // if cache is full, it's a capacity miss (capacity overrides conflict)
    // otherwise, it's a compulsory miss
    miss_e miss_type = compulsory;
    int set_is_full = 1;
    block_t *b;

    for (int i = 0; i < cache->associativity && set_is_full; i++) {
        b = (block_t *)cache->blocks + block_index + i;
        if (b->flags == empty_f) {
            set_is_full = 0;
        }
    }

    if (set_is_full) {
        miss_type = conflict;
    }

    int cache_is_full = 1;
    for (int i = 0; i < cache->block_count && cache_is_full; i++) {
        b = (block_t *)cache->blocks + i;
        if (b->flags == empty_f) {
            cache_is_full = 0;
        }
    }

    if (cache_is_full) {
        miss_type = capacity;
    }

    // call fetch from memory
    block = cache_fetch_from_memory(cache, address, memory);
    result = (cache_result_t) {
        .hit = 0,
        .miss_type = miss_type,
        .block = block,
        .requested_double = *(double *)((void *)block.data + offset),
        .requested_address = address
    };

    return result;
}

cache_result_t cache_store_addr(cache_t *cache, addr_t address, double a, memory_t *memory) {

    cache_result_t result = cache_load_addr(cache, address, memory);

    // update the line received directly in memory
    // get the offset into the block
    int offset = address % cache->block_size;
    block_t b = result.block;
    *(double *)((double *)b.data+(offset/8)) = a;

    result.requested_double = a;

    return result;
}

block_t cache_fetch_from_memory(cache_t *cache, addr_t address, memory_t *memory) {
    block_t block;

    // find the block_size-aligned array of doubles in memory
    addr_t aligned_addr = address - (address % cache->block_size);
    block = memory_fetch(aligned_addr, memory);

    // find the place in the cache it should be
    int offset_mask = cache->block_size -1;
    double offset_bits_d = log2((double) cache->block_size);
    int offset_bits = round(offset_bits_d);
    int set_count = cache->block_count / cache->associativity;
    int index_mask = set_count - 1;
    addr_t index = address >> offset_bits;
    index &= index_mask;
    int tag_mask = ~offset_mask;
    int tag = address & tag_mask;

    block.tag = tag;

    // look for an open block in the set
    int block_index = cache->associativity *index;
    block_t *block_cur;
    block_t *newly_set_block = NULL;
    int block_was_set = 0;
    for (int i = 0; i < cache->associativity; i++) {
        block_cur = (block_t *)cache->blocks + (block_index + i);
        if (block_cur->flags == empty_f) {
            // set proper replacement rank
            // both fifo and lru want newly loaded blocks at the head of the queue
            block.replacement_rank = cache->associativity;
            *block_cur = block;
            newly_set_block = block_cur;
            block_was_set = 1;
            break;
        }
    }

    // if the block was set, update the replacement rank of the other blocks
    // in the set, then return the newly placed block
    if (block_was_set) {
        for (int i = 0; i < cache->associativity; i++) {
            block_cur = (block_t *)cache->blocks + (block_index + i);
            if ((block_cur->flags != empty_f) && (block_cur != newly_set_block)) {
                block_cur->replacement_rank -= 1;
            }
        }

        return block;
    }

    // block wasn't set, so we have to evict one
    // choose a block to evict
    block_t *block_to_evict;
    switch (cache->brstrat) {
        case lru: { block_to_evict = find_lru(cache, block_index); break; }
        case fifo: { block_to_evict = find_oldest(cache, block_index); break; }
        case rando: { block_to_evict = find_random(cache, block_index); break; }
        default: { block_to_evict = find_random(cache, block_index); break; }
    }

    // make sure new block has the correct replacement rank
    // for misses, newly loaded blocks will always have replacement rank equal to
    // the cache associativity
    block.replacement_rank = cache->associativity;
    *block_to_evict = block;

    return block;
}

/* these functions handle all of the replacement logic. they find a block to
 * replace and update the replacement rank of the other blocks in the set.
 */
block_t *find_lru(cache_t *cache, int block_index) {
    block_t *lru_block = (block_t *)cache->blocks + block_index;
    block_t *test_block = lru_block;
    // find the blocks in the set
    // find the lowest replacement rank
    for (int i = 0; i < cache->associativity; i++) {
        test_block = (block_t *)cache->blocks + block_index + i;
        if (test_block->replacement_rank < lru_block->replacement_rank) {
            lru_block = test_block;
        }
    }

    // update the replacement rank on all of them
    for (int i = 0; i < cache->associativity; i++) {
        test_block->replacement_rank -= 1;
    }
    // grab that one
    return lru_block;
}

block_t *find_oldest(cache_t *cache, int block_index) {
    block_t *old_block = (block_t *)cache->blocks + block_index;
    block_t *test_block;
    // find the blocks in the set
    // find the lowest replacement rank
    for (int i = 0; i < cache->associativity; i++) {
        test_block = (block_t *)cache->blocks + block_index + i;
        if (test_block->replacement_rank < old_block->replacement_rank) {
            old_block = test_block;
        }
    }

    // update the replacement rank on all of them
    for (int i = 0; i < cache->associativity; i++) {
        test_block->replacement_rank -= 1;
    }

    // grab that one
    return old_block;
}

block_t *find_random(cache_t *cache, int block_index) {
    block_t *random_block;
    int rando_index = rand() % cache->associativity;

    random_block = (block_t *)cache->blocks + block_index + rando_index;

    return random_block;
}
