#include <stdlib.h>

#include "block.h"
#include "address.h"
#include "memory.h"

/* it's only important that the memory be at least twice as large as the
 * cache to attempt to faithfully simulate cache misses. it will take the
 * maximum valid address as a parameter. the max address is also the number
 * of bytes.
 */
memory_t *memory_init(addr_t max_addr) {
    memory_t *memory = (memory_t *) malloc(sizeof(memory_t));

    memory->data = (double (*) []) calloc(sizeof(char), max_addr);
    memory->max_addr = max_addr;

    return memory;
}

memory_t *memory_init_and_fill(addr_t max_addr) {
    memory_t *memory = memory_init(max_addr);

    int i;
    for (i = 0 ; i <= max_addr/8; i++) {
        (*memory->data)[i] = (double) i;
    }

    return memory;
}

void memory_free(memory_t *memory) {
    free(memory->data);
    free(memory);
    return;
}

block_t memory_fetch(addr_t address, memory_t *memory) {
    if (address > memory->max_addr) {
        return (block_t) {
            .head = 0x0000,
                .flags = empty_f,
                .tag = 0,
                .data = NULL
        };
    }

    // assumes address is already block-aligned
    // returns a pointer to the actual memory
    // have to do some pointer arithmetic
    //
    // address is the byte offset from the start of the memory array
    block_t block = {
        .head = address,
        .flags = valid_f,
        .tag = 0, // set by the cache
        .data = (double (*) []) ((void *)memory->data + address)
    };

    return block;
}
